"""
    Selenium client to create appointment on https://termin.corona-impfung.nrw/terminreservierung.
    
    Usage
    -----
    - Save this file to a writeable folder
    - Install requirements and adapt the configuration to your environment (see Requirements)
    - Open a new shell / commandline
    - Execute the script: 'python3 main.py "Emailadresse" "Passwort"'

    Requirements
    ------------
    - Windows (probably works on MacOS/Linux if the module winsound is removed, not tested)
    - Python 3.6+
    - Firefox
    - Geckodriver (copy to main.py folder)
    - Dependencies from requirements.txt
    - See "ToDo" tags further down for configurations to adapt

    Copyright (c) 2021 Karsten Hoffrath (khoffrath). All rights reserved.
"""
import sys
import codecs
import os
import datetime
import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, ElementClickInterceptedException, NoSuchElementException
import winsound

# ToDo: Set path to folder where main.py is located
root_folder = "c:/projects/impftermin/repo/python"

webdriver_executable = os.path.join(root_folder, "geckodriver.exe")
analysis_path = os.path.join(root_folder, "analysis")

do_restart = True

wait_for_element_timeout = 10
wait_for_element_timeout_short = 1
wait_for_popup_timeout = 3
wait_for_element_poll_frequency = 0.1
sleep_intervall_error_retry = 3
sleep_intervall_smoothing = 0.5

if not os.path.exists(analysis_path):
    os.mkdir(analysis_path)


def main(username, password):
    result = False
    while not result:
        try:
            driver = open_and_login(username, password)
            if webapp_navigation(driver):
                break
            if driver:
                save_browser_state(driver)
        except Exception as e:
            print("Error occurred, retrying...: " + str(e))
            if not do_restart:
                print("Don't restart, exiting")
                exit(1)
            try:
                if driver:
                    driver.close()
            except:
                pass
        time.sleep(1)


def open_and_login(username, password):
    # Login
    driver = webdriver.Firefox(executable_path=webdriver_executable)

    print("Loading login page")
    driver.get("https://signin.corona-impfung.nrw/adfs/oauth2/authorize?response_type=id_token%20token&response_mode=fragment&client_id=https%3A%2F%2FKvno.Impftermin.Portal&scope=openid&resource=https%3A%2F%2FKvno.Impftermin.Portal.WebApi&nonce=3548131270&pullStatus=1&redirect_uri=https%3A%2F%2Ftermin.corona-impfung.nrw%2Fhome")
    # Move browser window out of sight
    # driver.set_window_position(1500, 0, windowHandle='current');
    try:
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
            .until(EC.presence_of_element_located((By.ID, 'userNameInput')))
    except TimeoutException:
        print("Loading login page too much time!")
        driver.close()
        return False

    # Login
    print("Logging in with supplied username & password")
    elem = driver.find_element_by_id("userNameInput");
    elem.send_keys(username)
    elem = driver.find_element_by_id("passwordInput")
    elem.send_keys(password)
    elem = driver.find_element_by_id("submissionArea")
    elem.click()

    # Accept cookies
    print("Accepting cookies")
    try:
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
            .until(EC.presence_of_element_located((By.XPATH,
                                                   "/html/body/ngb-modal-window/div/div/app-confirm-cookies-dialog/div/button")))
        elem = driver.find_element_by_xpath("/html/body/ngb-modal-window/div/div/app-confirm-cookies-dialog/div/button")
    except TimeoutException:
        print("Waiting for cookie dialog took too much time!")
        driver.close()
        return False
    elem.click()
    return driver


# Navigate the open webpage, refreshing if necessary
def webapp_navigation(driver):
    # Timeout value for waiting for page content
    refreshed = False
    while True:
        if not refreshed:
            print("Check if booking allowed")
            try:
                WebDriverWait(driver, wait_for_element_timeout_short, wait_for_element_poll_frequency)\
                    .until(EC.presence_of_element_located((By.XPATH,
                                                           "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-zusammenfassung/div/section[1]/section/div")))
                elem = driver.find_element_by_xpath("/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-zusammenfassung/div/section[1]/section/div")
                if elem and elem.get_attribute("class") == "no-buchung-hinweis":
                    print("Not authorized, done")
                    driver.close()
                    return True
            except:
                pass

            print("Check for existing appointment")
            try:
                WebDriverWait(driver, wait_for_element_timeout_short, wait_for_element_poll_frequency)\
                    .until(EC.presence_of_element_located((By.XPATH,
                                                          "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-zusammenfassung/div/section[1]/section[1]/div/div[1]/div")))
                elem = driver.find_element_by_xpath("/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-zusammenfassung/div/section[1]/section[1]/div/div[1]/div")
                if elem and elem.get_attribute("class") == "selected-day legende-icon":
                    print("Appointment found, congratulation! Sending e-mail...")
                    elem = driver.find_element_by_xpath("/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-zusammenfassung/div/section[1]/section[1]/div/div[2]/div[1]/div[2]/button[1]")
                    elem.click()
                    print("Email sent, bye!")
                    driver.close()
                    return True
            except Exception as e:
                pass

            # Click booking button
            print("Searching booking button")
            try:
                WebDriverWait(driver, wait_for_element_timeout_short, wait_for_element_poll_frequency)\
                    .until(EC.presence_of_element_located((By.XPATH,
                                                           "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-zusammenfassung/div/section[1]/section/button")))
                elem = driver.find_element_by_xpath("/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-zusammenfassung/div/section[1]/section/button")
            except ElementClickInterceptedException:
                time.sleep(sleep_intervall_error_retry)
                elem.click()
            except TimeoutException:
                print("Waiting for booking button took too long!")
                driver.close()
                return False

            print("Clicking booking button")
            try:
                elem.click()
            except ElementClickInterceptedException:
                time.sleep(sleep_intervall_error_retry)
                elem.click()

        # Select single appointment
        print("Selecting single appointment")
        try:
            WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
                .until(EC.presence_of_element_located((By.XPATH, '//*[@id="0"]')))
            time.sleep(sleep_intervall_smoothing)
            elem = driver.find_element_by_xpath('//*[@id="0"]')
            elem.click()
        except TimeoutException:
            print("Waiting for single appointment element took too much time!")
            driver.close()
            return False
        except ElementClickInterceptedException:
            print("Retrying selecting single appointment")
            time.sleep(sleep_intervall_error_retry)
            elem.click()

        try:
            print("Jump to vaccination center page")
            WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
                .until(EC.presence_of_element_located((By.XPATH,
                                                       "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[1]/div/app-auswahl-buchungsart/fieldset/nav/div/button")))
            elem = driver.find_element_by_xpath("/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[1]/div/app-auswahl-buchungsart/fieldset/nav/div/button")
            elem.click()
        except TimeoutException:
            print("Waiting for Impfzentrum element took too much time!")
            driver.close()
            return False
        except ElementClickInterceptedException:
            print("Couldn't click Impfzentrum button")
            driver.close()
            return False

        try:
            # Jump to next page
            print("Jump to next page")
            WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
                .until(EC.presence_of_element_located((By.XPATH,
                                                       "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[2]/div/app-terminreservierung-impfzentrum/fieldset/div[2]/div/div/button[2]")))
            elem = driver.find_element_by_xpath("/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[2]/div/app-terminreservierung-impfzentrum/fieldset/div[2]/div/div/button[2]")
            time.sleep(sleep_intervall_smoothing)
            elem.click()
        except TimeoutException:
            print("Waiting for next page element took too much time!")
            driver.close()
            return False
        except ElementClickInterceptedException:
            print("Couldn't click button for next page, retrying...")
            time.sleep(sleep_intervall_smoothing)
            try:
                elem.click()
            except:
                driver.close()
                return False

        time.sleep(sleep_intervall_smoothing)

        print("Switching to calendar view")
        try:
            print("Waiting for calendar view...")
            WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency) \
                .until(EC.presence_of_element_located((By.XPATH,
                                                       "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[1]/fieldset[1]/div/div/div[2]/ngb-datepicker")))
        except TimeoutException:
            print("Waiting for calendar view took too much time!")
            driver.close()
            return False

        time.sleep(sleep_intervall_smoothing)

        try:
            # Disable pop up about no available appointments for this session
            #elem = driver.find_element_by_id("hideHinweis")
            #if elem:
            #    elem.click()
            print("Checking for notification because of no available appointments")
            WebDriverWait(driver, wait_for_popup_timeout, wait_for_element_poll_frequency)\
                .until(EC.presence_of_element_located((By.XPATH,
                                                       "/html/body/ngb-modal-window/div/div/app-terminbenachrichtigung/div/div[2]/div[2]/button[1]")))
            # Quick path: Popup about no available appointments -> refresh page and begin again
            print("No appointment available, retrying...")
            driver.refresh()
            refreshed = True
            continue
        except:
            # Modal dialog "Terminbenachrichtigung" invisible
            print("Modal dialog \"Terminbenachrichtigung\" invisible")
            pass

        (extraction_success, available_days) = search_available_appointments(driver)

        if not extraction_success:
            driver.close()
            return False

        if not available_days:
            print("No available appointment found, sleeping and retrying")
            driver.refresh()
            refreshed = True
            continue

        break

    if available_days:
        if create_appointment(driver, available_days):
            driver.close()
            return True
    return False


# Search in all available months for free appointments
def search_available_appointments(driver):
    available_days = []

    # Process current datetime picker
    result, appointments = process_datetime_picker(driver)
    for appointment in appointments:
        available_days.append(appointment)

    # No matter what, if we found at least one day with appointments return it
    if not result:
        if available_days:
            return True, available_days
        return False, available_days
    if available_days:
        return True, available_days

    # No appointment yet, check next months...
    # Change to next datetime picker if possible
    button_next = driver.find_element_by_id("weiter")
    # Scroll maximal two times to prevent the pop up to call the hotline
    max_scroll_forward = 2
    while button_next and max_scroll_forward > 0:
        max_scroll_forward -= 1
        time.sleep(sleep_intervall_smoothing)
        button_next.click()
        result, appointments = process_datetime_picker(driver)
        for appointment in appointments:
            available_days.append(appointment)
        # Break when appointment found, don't switch datetime picker
        if available_days:
            break
        try:
            WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
                .until(EC.presence_of_element_located((By.ID,
                                                       "weiter")))
            button_next = driver.find_element_by_xpath("weiter")
        except:
            pass

    return True, available_days


# Process one page with datetime pickers
def process_datetime_picker(driver):
    print("Searching for free appointments")
    elements = None
    months = []
    try:
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency) \
            .until(EC.presence_of_element_located((By.XPATH,
                                                   "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[1]/fieldset[1]/div/div/div[2]/ngb-datepicker")))
        date_pickers = driver.find_elements(By.XPATH,
                                            "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[1]/fieldset[1]/div/div/div[2]/ngb-datepicker")
        for date_picker in date_pickers:
            month_elements = date_picker.find_elements(By.CLASS_NAME, "ngb-dp-month")
            for month in month_elements:
                months.append(month)
    except Exception as e:
        print("Couldn't extract month elements: " + e)
        save_browser_state(driver, "couldnt_extract_month_elements")
        driver.close()
        return False, []

    if not months:
        print("No months")
        driver.close()
        return False, []

    days_with_appointment = []
    try:
        for month in months:
            WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency) \
                .until(EC.presence_of_element_located((By.CLASS_NAME,
                                                       "ngb-dp-week")))
            weeks = month.find_elements(By.CLASS_NAME, "ngb-dp-week")
            for week in weeks:
                # Weeks header
                if "ngb-dp-weekdays" in week.get_attribute("class"):
                    continue
                # Weeks data
                days = week.find_elements(By.XPATH, "./child::*")
                for day in days:
                    if "ngb-dp-day disabled hidden" in day.get_attribute("class") or \
                        "ngb-dp-day disabled" in day.get_attribute("class"):
                        # No valid day of month or no appointment available
                        continue
                    # Yeah, we found a day with available appointments
                    days_with_appointment.append(day)
    except Exception as e:
        print("Error extracting available appointments: " + e)
        save_browser_state(driver, "error_extracting_available_days")
        driver.close()
        return False, []

    return True, days_with_appointment


def create_appointment(driver, available_days):

    print("***********************************************************************")
    print("***********************************************************************")
    print("***********************************************************************")
    print("                          Found timeslots")
    print("***********************************************************************")
    print("***********************************************************************")
    print("***********************************************************************")

    day = available_days[0]
    time.sleep(sleep_intervall_smoothing)
    day.click()

    print("Searching button for time table first appointment")
    try:
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
            .until(EC.presence_of_element_located((By.XPATH,
                                                   "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[1]/fieldset[2]/div/div/fieldset/div[1]/label/div[2]/app-time-picker/div/div")))
    except:
        print("Couldn't find button, aborting")
        save_browser_state(driver, "couldnt_find_button_first_timetable")
        return False

    try:
        time.sleep(sleep_intervall_smoothing)
        print("Opening time table first appointment")
        elem = driver.find_element_by_xpath("/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[1]/fieldset[2]/div/div/fieldset/div[1]/label/div[2]/app-time-picker/div/div")
        if not elem:
            print("First time table not found")
            return False
        elem.click()

        print("Searching available timeslots")
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
            .until(EC.presence_of_element_located((By.XPATH,
                                                   "/html/body/ngb-popover-window/div[2]")))

    except:
        print("Time table first appointment not found")
        save_browser_state(driver, "time_table_first_appointment_not_found")
        return False

    elements = driver.find_elements_by_class_name("info-text")
    if elements:
        print("Time slots found: " + str(len(elements)))
        # Select latest timeslot
        print("Booking latest available timeslot")
        time.sleep(sleep_intervall_smoothing)
        elements[-1].click()
    else:
        save_browser_state(driver, "no_free_timeslots_first_appointment")
        return False

    print("Searching button for time table second appointment")
    try:
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
            .until(EC.presence_of_element_located((By.XPATH,
                                                   "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[1]/fieldset[2]/div/div/fieldset/div[2]/label/div[2]/app-time-picker/div/div")))
    except:
        print("Couldn't find button, aborting")
        save_browser_state(driver, "couldnt_find_button_second_timetable")
        return False


    try:
        time.sleep(sleep_intervall_smoothing)
        print("Opening time table second appointment")
        elem = driver.find_element_by_xpath(
            "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[1]/fieldset[2]/div/div/fieldset/div[2]/label/div[2]/app-time-picker/div/div")
        if not elem:
            print("Second time table not found")
            return False
        elem.click()

        print("Searching available timeslots")
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
            .until(EC.presence_of_element_located((By.XPATH,
                                                   "/html/body/ngb-popover-window/div[2]")))
    except:
        print("Time table second appointment not found")
        save_browser_state(driver, "time_table_second_appointment_not_found")
        return False

    elements = driver.find_elements_by_class_name("info-text")
    if elements:
        print("Time slots found: " + str(len(elements)))
        # Select latest timeslot
        print("Booking latest available timeslot")
        time.sleep(sleep_intervall_smoothing)
        elements[-1].click()
    else:
        save_browser_state(driver, "no_free_timeslots_second_appointment")
        return False

    # Timeslots reserved, booking appointment
    print("Creating appointment")
    try:
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
            .until(EC.presence_of_element_located((By.XPATH,
                                                   "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[2]/div/button")))
    except:
        print("Coulnd't find appointment booking button")
        save_browser_state(driver, "couldnt_find_appointment_booking_button")
        return False

    try:
        time.sleep(sleep_intervall_smoothing)
        elem = driver.find_element_by_xpath(
            "/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-terminreservierung/form/ngb-carousel/div/div[3]/div/app-terminreservierung-terminauswahl/div[2]/div/div[2]/div/button")

        if not elem:
            print("Appointment button not found")
            return False

        print("Appointment booking button found, clicking")
        elem.click()
    except:
        return False

    print("Confirm booking")
    try:
        WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
            .until(EC.presence_of_element_located((By.XPATH,
                                                   "/html/body/ngb-modal-window/div/div/app-terminauswahl-bestaetigung/div/div[2]/div[3]/button[2]")))
    except:
        print("Confirmation dialog not found")
        save_browser_state(driver, "couldnt_find_confirmation_button")
        return False

    try:
        time.sleep(sleep_intervall_smoothing)
        elem = driver.find_element_by_xpath("/html/body/ngb-modal-window/div/div/app-terminauswahl-bestaetigung/div/div[2]/div[3]/button[2]")

        if not elem:
            print("Couldn't find confirmation dialog button")
            return False

        print("Confirmation button found, clicking")
        elem.click()
    except:
        return False

    print("Appointment booked, congratulations! Done...")
    winsound.PlaySound(None, winsound.SND_LOOP + winsound.SND_ASYNC)

    return True


# Persist the current browser state for later analysis
def save_browser_state(driver, name_postfix=""):
    try:
        timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        print("-----------------------------------------")
        print("              " + timestamp)
        print("-----------------------------------------")
        filename = name_postfix
        if name_postfix:
            filename = "_" + filename
        filename = os.path.join(analysis_path, timestamp + filename)
        image_filename = filename + ".png"
        source_filename = filename + ".html"
        driver.save_screenshot(image_filename)
        file = codecs.open(source_filename, "w", "utf-8")
        file.write(driver.page_source)
        file.close()
    except Exception as e:
        print("Error creating debug files: " + e)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('Usage:')
        print('python api_runner.py "Emailadresse" "Passwort"')
        sys.exit(1)
    main(username=sys.argv[1],
         password=sys.argv[2])

"""
    REST-Api client for https://termin.corona-impfung.nrw/terminreservierung.

    Copyright (c) 2021 Karsten Hoffrath (khoffrath). All rights reserved.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

    The latest version can be found here: https://gitlab.com/khoffrath/covid19-impf-o-mat

    Code quality: PoC ¯\_(ツ)_/¯
"""
import datetime
import time
import urllib
import uuid

import requests
from dateutil import parser


# Authentication failed
class AuthorizeException(Exception):
    pass


# Token issue
class TokenFailureException(Exception):
    pass


# Backend not ok
class BackendNotOkException(Exception):
    pass


# Miscancellous errors
class MiscancellousErrorException(Exception):
    def __init__(self, message, can_continue):
        super().__init__(message)
        self.can_continue = can_continue


# API client class
class ApiClient:
    # Default wait intervall
    default_wait_intervall = 0.2

    # Number of retries before aborting in case of backend errors (backend_ok == False)
    retries_backend_error = 180  # 15 minutes with sleep_time_backend_error == 5
    # Seconds to sleep before retrying in case of backend errors (backend_ok == False)
    sleep_time_backend_error = 5

    # Seconds to sleep between attempts to fetch available timestamps
    sleep_time_get_list_freie_tage = 0.4

    # Seconds to sleep after available timeslots were found but we failed to create
    # an appointment
    sleep_time_after_failed_appointment_creation = 1

    # Seconds to sleep after an unknown exception
    sleep_time_out_general_exception = 5

    # Seconds to sleep after token refresh errors
    sleep_time_token_refresh_error = 10

    # Request timeout value
    request_timeout = 5.0

    nonce = None
    username = None
    password = None
    earliest_timestamp = None
    request_id = str(uuid.uuid4())
    recaptcha_sitekey = None
    cookies = None
    bearer_token = None
    only_log_appointments = False

    _state = False
    _message = None

    # Constructor
    def __init__(self, nonce, username, password, earliest_timestamp, only_log_appointments=False):
        self.nonce = nonce
        self.username = username
        self.password = password
        self.earliest_timestamp = earliest_timestamp
        self.only_log_appointments = only_log_appointments

    @property
    def result(self):
        print("Getting  result...")
        return {
            'state': self._state,
            'message': self._message
        }

    # Execute the client
    def execute(self):
        authenticated = self._authenticate()
        if not authenticated:
            self._state = False
            self._message = 'Username or password wrong. Aborting'
            return False

        self.recaptcha_sitekey = self._get_recaptcha_sitekey()

        while True:
            self._prepare_members()
            try:
                self._preparation_calls()

                time.sleep(self.default_wait_intervall)

                # Get all ordnungsnummern to process
                temp_list = []
                for benutzer in self.benutzer:
                    if benutzer['HasBuchung']:
                        continue
                    temp_list.append(benutzer['Ordnungsnummer'])

                # Extract all ordnungsnummern allowed to create appointments (double-check against benutzer['HasBuchung'])
                list_ordnungsnummern_to_process = []
                for ordnungsnummer in temp_list:
                    time.sleep(self.default_wait_intervall)
                    if self._get_isrisikogruppe(ordnungsnummer):
                        list_ordnungsnummern_to_process.append(ordnungsnummer)

                # Check if any ordnungsnummer is allowed to create an appointment
                # and we can continue, otherwise abort.
                if not list_ordnungsnummern_to_process:

                    # Force loop until one appointment was created
                    # 2021-06-26: Loop until appointments become available
                    """
                    print(f"{datetime.datetime.now()}: No user permitted to create an appointment, sleeping and retrying...")
                    time.sleep( self.sleep_time_after_failed_appointment_creation)
                    continue
                    """
                    self._state = False
                    self._message = 'Every user already has an appointment or no user is permitted to create an appointment.'
                    return False

                # Try to create an appointment for every ordnungsnummer
                temp_list = list_ordnungsnummern_to_process[:]
                for ordnungsnummer in temp_list:
                    print(f'User ordnungsnummer: {ordnungsnummer}')
                    if self._loop_until_appointment(ordnungsnummer):
                        list_ordnungsnummern_to_process.remove(ordnungsnummer)

                # Appointments for all ordnungsnummern created, done
                if not list_ordnungsnummern_to_process:
                    self._state = True
                    self._message = 'Every profile has an appointment, done'
                    return True

                # Once we're here the loop restarts to check if there are more
                # ordnungsnummern to process. If not we are done and the loop
                # exits.

            except AuthorizeException:
                authenticated = self._authenticate()
                if not authenticated:
                    self._state = False
                    self._message = 'Authentication failed'
                    return False
            except TokenFailureException:
                authenticated = self._authenticate()
                while not authenticated:
                    # Since we get to this point we were successfully authenticate
                    # at one point (the given username and password are correct).
                    # Sleep and retry
                    time.sleep(self.sleep_time_token_refresh_error)
                    authenticated = self._authenticate()
            except BackendNotOkException:
                retry_counter = self.retries_backend_error
                while retry_counter >= 0:
                    if self._authenticate() and self._get_backend_ok():
                        break
                    time.sleep(self.sleep_time_backend_error)
            except MiscancellousErrorException as e:
                if not e.can_continue:
                    self._state = False
                    self._message = e.message
                    return False
            except TimeoutError:
                print('Timeout, retrying without sleep')
            except requests.exceptions.RequestException:
                print('RequestException, retrying without sleep')
            except ConnectionError:
                print('Connection error, retrying without sleep')
            except Exception as e:
                print("Exception, retrying after sleep timeout: {0}".format(e))
                time.sleep(self.sleep_time_out_general_exception)

    # Signal if the given timeslot is acceptable
    def _timeslot_acceptable(self, timeslot: str) -> bool:
        if not self.earliest_timestamp:
            print('No earliest timestamp given, accepting found timeslot')
            return True
        print('Earliest timestamp to accept: {0}'.format(self.earliest_timestamp))
        found_timestamp = parser.parse(timeslot)
        print('found_timestamp: {0}'.format(timeslot))
        print('found_timestamp >= self.earliest_timestamp: {0}'.format(found_timestamp >= self.earliest_timestamp))
        return found_timestamp >= self.earliest_timestamp

    # Initialize class members
    def _prepare_members(self):
        self.user = None
        self.settings = None
        self.backend_ok = None
        self.benutzer = None
        self.buchungs_status = None
        self.is_risiko_gruppe = None
        self.buchung_summary = None
        self.max_profile_count = None
        self.profiles = None
        self.list_impfzentrum = None
        self.current_benutzer_index = 0

    # Collect all needed data
    def _preparation_calls(self):
        self.user = self._get_user()
        self.settings = self._get_settings()
        self.backend_ok = self._get_backend_ok()
        self.benutzer = self._get_benutzer()
        self.buchungs_status = self._get_buchung_status()
        self.buchung_summary = self._get_buchung_summary()
        self.max_profile_count = self._get_MaxProfileCount()
        self.profiles = self._get_profiles()

    def _loop_until_appointment(self, ordnungsnummer):

        # Check backend
        self.backend_ok = self._get_backend_ok()
        if not self.backend_ok:
            raise BackendNotOkException('Backend not ok')

        if self.only_log_appointments:
            print('Only logging open appointments...')

        # Fetch list of impfzentren
        self.list_impfzentrum = self._get_list_impfzentrum(
            number_user_profile=ordnungsnummer)

        if not self.list_impfzentrum:
            raise MiscancellousErrorException(message='No impfzentrum for ordnungsnummer {0}'.format(ordnungsnummer),
                                              can_continue=False)

        # List of impfzentren available, now run a loop until an appointment
        # was created
        impfzentrum = self.list_impfzentrum[0]
        print(f'Impfzentrum: {impfzentrum}')
        while True:
            backend_ok = self._get_backend_ok()
            if not backend_ok:
                raise BackendNotOkException('Backend not ok')

            # Get list of available days
            list_freie_tage = self._get_list_freie_tage(impfzentrum_id=impfzentrum['Id'],
                                                        start_date=datetime.date.today(),
                                                        end_date=datetime.date(year=2021, month=10, day=31))
            if not list_freie_tage:
                if not self.only_log_appointments:
                    print('{0} - Waiting for available days'.format(datetime.datetime.now()))
                time.sleep(self.sleep_time_get_list_freie_tage)
                continue

            if self.only_log_appointments:
                continue

            print('{0} - Available day(s) found: {1}'.format(datetime.datetime.now(), list_freie_tage))
            # Day(s) with free timeslots available, try all days until an appointment
            # was created or we run out of days
            for day in list_freie_tage:
                if not day['HasFreienTagTermin2']:
                    # No timeslot available for second appointment, skip the day
                    continue

                print('Found day with available second appointment: {0}'.format(day))

                zeitslots_structure = self._get_timeslots(day['FreierTagId'])

                if zeitslots_structure:
                    print('Found available timeslots, trying to create an appointment')
                    # We have a structure with available timeslots, iterate over all available
                    # timeslots until an appointment was created or we run out of timeslots
                    for hour_slot in zeitslots_structure:
                        if hour_slot['ZeitSlotRanges']:
                            for zeitslot_range in hour_slot['ZeitSlotRanges']:
                                if zeitslot_range['ZeitSlots']:
                                    for zeitslot in zeitslot_range['ZeitSlots']:
                                        print('Found available timeslot: {0}'.format(zeitslot))
                                        if not self._timeslot_acceptable(zeitslot['Datum']):
                                            print('Timeslot not accepted, trying next')
                                            continue
                                        # We found an available timeslot, try to create an appointment
                                        if self._try_create_appointment(ordnungsnummer=ordnungsnummer,
                                                                        impfzentrum=impfzentrum,
                                                                        zeitslot=zeitslot):
                                            # Yeah, we created an appointment. Mail was sent, we're done!
                                            return
                else:
                    print('No available timeslots found, retrying')
            # No appointment created, sleep and restart the loop
            time.sleep(self.sleep_time_after_failed_appointment_creation)

    def _try_create_appointment(self, ordnungsnummer, impfzentrum, zeitslot):
        # Pre-flight POST to create a request-id
        # {"Anzahl":1,"ImpfzentrumId":15,"ReservierungsIds":[],"ZeitraumSlot1Von":"2021-05-30T17:30:00.000Z"}
        zeitraum_slot1_von = zeitslot['Datum'] # Calculate Z-time?
        json_data = {
            'Anzahl': 1,
            'ImpfzentrumId': impfzentrum['Id'],
            'ReservierungsIds': [],
            'ZeitraumSlot1Von': zeitraum_slot1_von
        }
        response_preflight = self._post_reservation(json_data=json_data)
        if not response_preflight:
            # No JSON response received, abort
            print('Reservation call for first appointment failed')
            return False

        if not response_preflight['Reservierungen']:
            # Invalid JSON response, abort
            print("Couldn't create reservation first appointment. Response: {0}".format(response_preflight))
            return False

        # Response pre-flight: {"Reservierungen":[{"ReservierungsId":"d57bbf48-5c5e-44e2-9dc4-cd297a84a2aa","Termin1":"2021-05-30T19:35:00+02:00","TerminId1":813489,"Termin2":null,"TerminId2":null}]}
        print("First appointment created")

        # Create a reservation
        zeitraum_slot1_von = response_preflight['Reservierungen'][0]['Termin1'] # Calculate Z-time?
        timeslot_1 = parser.parse(response_preflight['Reservierungen'][0]['Termin1'])
        timeslot_2 = timeslot_1 + datetime.timedelta(days=impfzentrum['OffsetZweiterTermin'])
        zeitraum_slot2_von = timeslot_2.strftime('%Y-%m-%dT%I:%M:%S%z')
        json_data = {
            'Anzahl': 1,
            'ImpfzentrumId': impfzentrum['Id'],
            'ReservierungsIds': [response_preflight['Reservierungen'][0]['ReservierungsId']],
            'ZeitraumSlot1Von': zeitraum_slot1_von,
            'ZeitraumSlot2Von': zeitraum_slot2_von
        }

        response_reservation = self._post_reservation(json_data=json_data)
        if not response_reservation:
            # No JSON response received, abort
            print('Reservation call for second appointment failed')
            return False

        if not response_reservation['Reservierungen']:
            # Invalid JSON response, abort
            print("Couldn't create reservation second appointment. Response: {0}".format(response_reservation))
            return False

        # Response reservation: {"Reservierungen":[{"ReservierungsId":"d57bbf48-5c5e-44e2-9dc4-cd297a84a2aa","Termin1":"2021-05-30T19:35:00+02:00","TerminId1":813489,"Termin2":"2021-07-11T19:40:00+02:00","TerminId2":942353}]}

        # Book the reservation
        json_data = {
            'Buchungen':
                [
                    {
                        'ImpfzentrumId': impfzentrum['Id'],
                        'FreierTermin1Id': response_reservation['Reservierungen'][0]['TerminId1'],
                        'FreierTermin2Id': response_reservation['Reservierungen'][0]['TerminId2'],
                        'Risikogruppen': [],
                        'ReservierungId': response_reservation['Reservierungen'][0]['ReservierungsId'],
                        'Ordnungsnummer': ordnungsnummer
                    }
                ]
        }
        response_booking = self._post_buchen(json_data=json_data)

        if not response_booking:
            # No JSON response received, abort
            print('Call to book appointments failed')
            return False

        if not response_booking['EmailSentSuccessfully']:
            # Invalid JSON response, abort
            print("Couldn't book appointments. Response: {0}".format(response_booking))
            return False

        print('Appointments booked')

        response_benutzer = self._get_benutzer()
        if response_benutzer:
            # [{"Id":"46926bcd-cda0-4d15-bd94-1607de8207b1","SId":"S-1-5-21-2597659853-2504274404-1331653712-1892430","Ordnungsnummer":1,"Email":"khoffrath@khoffrath.de","Anrede":1,"Titel":null,"Vorname":"Karsten","Nachname":"Hoffrath","Geburtsdatum":"1974-01-18T00:00:00+01:00","Geschlecht":1,"Versichertennummer":null,"Adresszusatz":null,"Strasse":"Zum Kannebach 22","Plz":"42553","Ort":"Velbert","Vorwahl":"0160","Rufnummer":"2393269","HasBuchung":true,"ZustimmungImAuftrag":null,"NichtPriorisiert":false,"NeueTermineEmailSubscription":true,"SonderStiko":4}]
            # Search user profile with current ordnungsnummer
            for benutzer in response_benutzer:
                if benutzer['Ordnungsnummer'] == ordnungsnummer:
                    if benutzer['HasBuchung'] and not response_booking['EmailSentSuccessfully']:
                        if self._get_send_email(ordnungsnummer=ordnungsnummer):
                            print('Notification mail re-sent')
                        else:
                            print('Re-sending of notification mail failed')
        return True


    # Get the sitekey for recaptcha
    def _get_recaptcha_sitekey(self):
        headers = {
            'Host': 'termin.corona-impfung.nrw',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/json',
            'DNT': '1',
            'Connection': 'keep-alive',
            'Referer': 'https://termin.corona-impfung.nrw/home'
        }
        response = requests.get(url='https://termin.corona-impfung.nrw/api/settings/recaptcha/sitekey',
                                headers=headers,
                                timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('Response status code "{0}"'.format(response.status_code))

    # Authenticate / Re-authenticate
    def _authenticate(self):
        POST_URL = 'https://signin.corona-impfung.nrw/adfs/oauth2/authorize'

        POST_URL_PARAMS = {
            "response_type": "id_token token",
            "response_mode": "fragment",
            "client_id": "https://Kvno.Impftermin.Portal",
            "scope": "openid",
            "resource": "https://Kvno.Impftermin.Portal.WebApi",
            "nonce": self.nonce,
            "pullStatus": "1",
            "redirect_uri": "https://termin.corona-impfung.nrw/home",
            "client-request-id": self.request_id
        }

        POST_REQUEST_HEADERS = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': 'https://signin.corona-impfung.nrw',
            'DNT': '1',
            'Referer': urllib.parse.urljoin(POST_URL, urllib.parse.urlencode(POST_URL_PARAMS)),
            'Upgrade-Insecure-Requests': '1'
        }

        POST_DATA = {
            'UserName': self.username,
            'Password': self.password,
            'AuthMethod': 'FormsAuthentication'
        }

        post_response = requests.post(url=POST_URL, params=POST_URL_PARAMS,
                                      data=POST_DATA, headers=POST_REQUEST_HEADERS,
                                      allow_redirects=False,
                                      timeout=self.request_timeout)

        if post_response.status_code != 302:
            print("Authorize POST status code: {0}, aborting".format(post_response.status_code))
            return False

        self.cookies = post_response.cookies

        GET_URL = 'https://signin.corona-impfung.nrw/adfs/oauth2/authorize'

        GET_URL_PARAMS = {
            'response_type': 'id_token token',
            'response_mode': 'fragment',
            'client_id': 'https://Kvno.Impftermin.Portal',
            'scope': 'openid',
            'resource': 'https://Kvno.Impftermin.Portal.WebApi',
            'nonce': self.nonce,
            'pullStatus': '1',
            'redirect_uri': 'https://termin.corona-impfung.nrw/home',
            'client-request-id': self.request_id
        }

        GET_REQUEST_HEADERS = {
            'Host': 'signin.corona-impfung.nrw',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': 'https://signin.corona-impfung.nrw',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Sec-GPC': '1',
            'Referer': urllib.parse.urljoin(GET_URL, urllib.parse.urlencode(GET_URL_PARAMS)),
        }

        get_response = requests.get(url=GET_URL, params=GET_URL_PARAMS,
                                    headers=GET_REQUEST_HEADERS, cookies=self.cookies,
                                    allow_redirects=False,
                                    timeout=self.request_timeout)

        if get_response.status_code != 302:
            print("Authorize GET status code: {0}, aborting".format(get_response.status_code))
            return False

        try:
            # Response header 'location': parameter 'access_token' extrahieren und speichern zur Verwendung als Bearer Token
            parse_result = urllib.parse.urlparse(get_response.headers['location'])
            parse_qs_result = urllib.parse.parse_qs(parse_result.fragment)
            self.bearer_token = parse_qs_result['access_token'][0]
            self.cookies = get_response.cookies
            return True
        except Exception as ex:
            print('Failed to authenticate: {0}'.format(ex))
            return False

    # Get request headers with bearer token
    def _get_authentication_request_headers(self, referer='', origin=''):
        headers = {
            'Host': 'termin.corona-impfung.nrw',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Authorization': 'Bearer {0}'.format(self.bearer_token),
            'Content-Type': 'application/json',
            'DNT': '1',
            'Connection': 'keep-alive',
            'TE': 'Trailers'
        }
        if referer:
            headers['Referer'] = referer
        if origin:
            headers['Origin'] = origin
        return headers

    # Get user data
    # Response: {"Upn":"788f80ed-c6cc-444c-bc8d-f9a4e70ab75d@kvno-ia-live.local","Sid":null,"UserRoles":[2],"Name":null,"Email":"khoffrath@khoffrath.de","UserName":"Karsten Hoffrath","FirstName":"Karsten","LastName":"Hoffrath"}
    def _get_user(self):
        response = requests.get(url='https://termin.corona-impfung.nrw/api/user',
                                headers=self._get_authentication_request_headers(
                                    'https://termin.corona-impfung.nrw/home'),
                                timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_user() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException("_get_user()")

    # Get settings
    # Response: {"MaxProfileCount":"2","Terminreservierung:Altersgrenze":"70","Terminreservierung:AltersgrenzeBeruflicheIndikation":"18","Terminreservierung:AltersgrenzeMedizinischeIndikation":"18","Terminreservierung:BenachrichtigungsHinweisTimeoutInSeconds":"180","Terminreservierung:CalendarNavigationCountForHinweis":"4","Terminreservierung:GemeinschaftsterminOffset":"30","Terminreservierung:TerminreservierungTimeoutInSeconds":"360","Anonymous:LinkConfiguration:AbsenderMail":"noreply@corona-impfung.nrw","Anonymous:LinkConfiguration:Anmelden":"/terminregistrierung","Anonymous:LinkConfiguration:DownloadLinkFirefox":"https://www.mozilla.org/de/firefox/new/","Anonymous:LinkConfiguration:DownloadLinkInternetExplorer":"https://www.microsoft.com/de-de/download/details.aspx?id=41628","Anonymous:LinkConfiguration:GoogleDatenschutz":"https://policies.google.com/privacy","Anonymous:LinkConfiguration:PrivacyShield":"https://www.privacyshield.gov/list","Anonymous:LinkConfiguration:RegistrierungAbbrechenRedirect":"https://www.kvno.de","Anonymous:LinkConfiguration:Zugangvergessen":"/passwortvergessen","Adfs:AcquireTokenRoute":"adfs/oauth2/token","Adfs:AnonymousRoutes":"/registrierung;/emailbestaetigung;/home;/registrierung/confirmemail;/nutzungsbedingungen;/datenschutz;/resetpassword;/passwortupdate","Adfs:AuthorizationRoute":"adfs/oauth2/authorize","Adfs:ClientId":"https://Kvno.Impftermin.Portal","Adfs:IsRootUrlAnonymous":"True","Adfs:LogoutRoute":"adfs/oauth2/logout","Adfs:RedirectAfterLogoutUrl":"https://kvno.de","Adfs:ServerUrl":"https://signin.corona-impfung.nrw/","Adfs:SessionValidityMinutes":"15","Adfs:SsoRefreshHelper":"-","Adfs:WebAppId":"https://Kvno.Impftermin.Portal.WebApi"}
    def _get_settings(self):
        response = requests.get(url='https://termin.corona-impfung.nrw/api/settings/',
                                headers=self._get_authentication_request_headers(
                                    'https://termin.corona-impfung.nrw/zusammenfassung'),
                                timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_settings() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException("_get_settings()")

    # Get backend ok
    # Result: True or False
    def _get_backend_ok(self):
        response = requests.get(url='https://termin.corona-impfung.nrw/api/impfling/buchung/backend/ok',
                                headers=self._get_authentication_request_headers(
                                    'https://termin.corona-impfung.nrw/zusammenfassung'),
                                timeout=self.request_timeout)
        if response.status_code == 200:
            if response.text == 'true':
                return True
            return False
        print('_get_backend_ok() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_backend_ok()')

    # Get benutzer
    # Response: [{"Id":"46926bcd-cda0-4d15-bd94-1607de8207b1","SId":"S-1-5-21-2597659853-2504274404-1331653712-1892430","Ordnungsnummer":1,"Email":"khoffrath@khoffrath.de","Anrede":1,"Titel":null,"Vorname":"Karsten","Nachname":"Hoffrath","Geburtsdatum":"1974-01-18T00:00:00+01:00","Geschlecht":1,"Versichertennummer":null,"Adresszusatz":null,"Strasse":"Zum Kannebach 22","Plz":"42553","Ort":"Velbert","Vorwahl":"0160","Rufnummer":"2393269","HasBuchung":false,"ZustimmungImAuftrag":null,"NichtPriorisiert":false,"NeueTermineEmailSubscription":true,"SonderStiko":4}]
    def _get_benutzer(self):
        response = requests.post(url='https://termin.corona-impfung.nrw/api/benutzer/benutzer',
                                 headers=self._get_authentication_request_headers(
                                     'https://termin.corona-impfung.nrw/zusammenfassung'),
                                 timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_benutzer() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_benutzer()')

    # Get buchung status
    # Response: {'1':0, '2':1}
    def _get_buchung_status(self):
        response = requests.get(url='https://termin.corona-impfung.nrw/api/impfling/buchung/status',
                                headers=self._get_authentication_request_headers(
                                    'https://termin.corona-impfung.nrw/zusammenfassung'),
                                timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_buchung_status() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_buchung_status()')

    # Get isrisikogruppe
    # Response: [{"Ordnungsnummer":1,"IsBerechtigterBenutzer":true}]
    def _get_isrisikogruppe(self, ordnungsnummer):
        # Number of user profile
        post_data = '[{0}]'.format(ordnungsnummer)
        response = requests.post(url='https://termin.corona-impfung.nrw/api/impfling/isrisikogruppe',
                                 headers=self._get_authentication_request_headers(
                                     'https://termin.corona-impfung.nrw/zusammenfassung',
                                     'https://termin.corona-impfung.nrw'),
                                 data=post_data,
                                 timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_isrisikogruppe() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_isrisikogruppe()')

    # Get buchung summary
    # Response: [] oder [{"Impfzentrum":{"Id":15,"Name":"IZ Mettmann Landkreis - mRNA","Strasse":"Timocom-Platz 1","Plz":"40699","Ort":"Erkrath","OffenAb":"2021-02-01T00:00:00+01:00","OffsetZweiterTermin":42},"Termin1Id":4569444,"Termin2Id":4569445,"Termin1":"2021-05-30T19:35:00+02:00","Termin2":"2021-07-11T19:40:00+02:00","Termin1Status":0,"Termin2Status":0,"Ordnungsnummer":1}]
    def _get_buchung_summary(self):
        response = requests.get(url='https://termin.corona-impfung.nrw/api/impfling/buchung/summary',
                                headers=self._get_authentication_request_headers(
                                    'https://termin.corona-impfung.nrw/zusammenfassung'),
                                timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_buchung_summary() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_buchung_summary()')

    # Get MaxProfileCount
    # Response: {'MaxProfileCount': '2'}
    def _get_MaxProfileCount(self):
        response = requests.get(url='https://termin.corona-impfung.nrw/api/settings/MaxProfileCount',
                                headers=self._get_authentication_request_headers(
                                    'https://termin.corona-impfung.nrw/profil'),
                                timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_MaxProfileCount() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_MaxProfileCount()')

    # Get profiles
    # Response:
    def _get_profiles(self):
        response = requests.get(url='https://termin.corona-impfung.nrw/api/benutzer/profiles',
                                headers=self._get_authentication_request_headers(
                                    'https://termin.corona-impfung.nrw/profil'),
                                timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_profiles() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_profiles()')

    # Save profile
    def _save_profile(self):
        # Won't work because of Google reCaptcha
        pass

    # Get list Impfzentrum
    # Response: [{"Id":15,"Name":"IZ Mettmann Landkreis - mRNA","Strasse":"Timocom-Platz 1","Plz":"40699","Ort":"Erkrath","OffenAb":"2021-02-01T00:00:00+01:00","OffsetZweiterTermin":42}]
    def _get_list_impfzentrum(self, number_user_profile):
        # Number of user profile
        post_data = '[{0}]'.format(number_user_profile)
        response = requests.post(url='https://termin.corona-impfung.nrw/api/impfzentrum/list',
                                 headers=self._get_authentication_request_headers(
                                     referer='https://termin.corona-impfung.nrw/terminreservierung',
                                     origin='https://termin.corona-impfung.nrw'),
                                 data=post_data,
                                 timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_list_impfzentrum() response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_list_impfzentrum()')

    # Get list freie Tage
    # Response: [] oder [{"FreierTagId":26332,"Datum":"2021-05-30T00:00:00+02:00","HasFreienTagTermin2":true},{"FreierTagId":27189,"Datum":"2021-07-11T00:00:00+02:00","HasFreienTagTermin2":false}]
    def _get_list_freie_tage(self, impfzentrum_id, start_date, end_date):
        # {"Bis":"2021-10-31","Von":"2021-5-23","ImpfzentrumId":15}
        post_data = {
            'Von': '{0}-{1}-{2}'.format(start_date.year, start_date.month, start_date.day),
            'Bis': '{0}-{1}-{2}'.format(end_date.year, end_date.month, end_date.day),
            'ImpfzentrumId': impfzentrum_id
        }
        response = requests.post(url='https://termin.corona-impfung.nrw/api/impfzentrum/freietage',
                                 headers=self._get_authentication_request_headers(
                                     referer='https://termin.corona-impfung.nrw/terminreservierung',
                                     origin='https://termin.corona-impfung.nrw'),
                                 json=post_data,
                                 timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_list_freie_tage response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_list_freie_tage')

    # Get time slot
    # Response: [{"Stunde":9,"ZeitSlotRanges":[{"Von":"2021-05-30T09:00:00+02:00","Bis":"2021-05-30T09:29:00+02:00","ZeitSlots":[{"FreierTerminId":813363,"Datum":"2021-05-30T09:05:00+02:00"},{"FreierTerminId":813365,"Datum":"2021-05-30T09:15:00+02:00"}]},{"Von":"2021-05-30T09:30:00+02:00","Bis":"2021-05-30T09:59:00+02:00","ZeitSlots":[]}]},{"Stunde":12,"ZeitSlotRanges":[{"Von":"2021-05-30T12:00:00+02:00","Bis":"2021-05-30T12:29:00+02:00","ZeitSlots":[{"FreierTerminId":813402,"Datum":"2021-05-30T12:20:00+02:00"}]},{"Von":"2021-05-30T12:30:00+02:00","Bis":"2021-05-30T12:59:00+02:00","ZeitSlots":[]}]},{"Stunde":15,"ZeitSlotRanges":[{"Von":"2021-05-30T15:00:00+02:00","Bis":"2021-05-30T15:29:00+02:00","ZeitSlots":[{"FreierTerminId":813436,"Datum":"2021-05-30T15:10:00+02:00"},{"FreierTerminId":813438,"Datum":"2021-05-30T15:20:00+02:00"}]},{"Von":"2021-05-30T15:30:00+02:00","Bis":"2021-05-30T15:59:00+02:00","ZeitSlots":[{"FreierTerminId":813442,"Datum":"2021-05-30T15:40:00+02:00"}]}]},{"Stunde":17,"ZeitSlotRanges":[{"Von":"2021-05-30T17:00:00+02:00","Bis":"2021-05-30T17:29:00+02:00","ZeitSlots":[]},{"Von":"2021-05-30T17:30:00+02:00","Bis":"2021-05-30T17:59:00+02:00","ZeitSlots":[{"FreierTerminId":813465,"Datum":"2021-05-30T17:35:00+02:00"},{"FreierTerminId":813466,"Datum":"2021-05-30T17:40:00+02:00"},{"FreierTerminId":813468,"Datum":"2021-05-30T17:50:00+02:00"}]}]},{"Stunde":19,"ZeitSlotRanges":[{"Von":"2021-05-30T19:00:00+02:00","Bis":"2021-05-30T19:29:00+02:00","ZeitSlots":[]},{"Von":"2021-05-30T19:30:00+02:00","Bis":"2021-05-30T19:59:00+02:00","ZeitSlots":[{"FreierTerminId":813489,"Datum":"2021-05-30T19:35:00+02:00"}]}]}]
    def _get_timeslots(self, day_id):
        response = requests.get(
            url='https://termin.corona-impfung.nrw/api/impfzentrum/zeitslots/{0}'.format(day_id),
            headers=self._get_authentication_request_headers(
                referer='https://termin.corona-impfung.nrw/terminreservierung'),
            timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_get_timeslots response status code "{0}"'.format(response.status_code))
        raise TokenFailureException('_get_timeslots()')

    # Post JSON data to endpoint reservieren
    def _post_reservation(self, json_data):
        response = requests.post(url='https://termin.corona-impfung.nrw/api/buchung/termin/reservieren',
                                 headers=self._get_authentication_request_headers(
                                     referer='https://termin.corona-impfung.nrw/terminreservierung',
                                     origin='https://termin.corona-impfung.nrw'),
                                 json=json_data,
                                 timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_post_reservation() response status code "{0}"\n{1}'.format(response.status_code, json_data))
        raise TokenFailureException('_post_reservation()')

    # Post JSON data to endpoint buchen
    def _post_buchen(self, json_data):
        response = requests.post(url='https://termin.corona-impfung.nrw/api/buchung/termin/buchen',
                                 headers=self._get_authentication_request_headers(
                                     referer='https://termin.corona-impfung.nrw/terminreservierung',
                                     origin='https://termin.corona-impfung.nrw'),
                                 json=json_data,
                                 timeout=self.request_timeout)
        if response.status_code == 200:
            return response.json()
        print('_post_buchen() response status code "{0}"\n{1}'.format(response.status_code, json_data))
        raise TokenFailureException('_post_buchen()')

    # Sent a confirmation mail for the given ordnungsnummer
    # Result: Successfully sent
    def _get_send_email(self, ordnungsnummer):
        response = requests.get(url='https://termin.corona-impfung.nrw/api/impfling/buchung/summarymail/1',
                                headers=self._get_authentication_request_headers(
                                    referer='https://termin.corona-impfung.nrw/zusammenfassung'
                                ),
                                timeout=self.request_timeout)
        if response.status_code == 200:
            return response.text == 'Summary-Mail was sent.'
        print('_get_send_email() response status code "{0}"'.format(response.status_code))
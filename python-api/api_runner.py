"""
    REST-Api client to create appointments on https://termin.corona-impfung.nrw/terminreservierung.

    Usage
    -----
    - Save this file to a writeable folder
    - Install requirements and adapt the configuration to your environment (see "Requirements")
    - Optionally set the earliest timestamp to be accepted, see "earliest_timestamp" in method "run".
    - Open a new shell / commandline
    - Execute the script: 'python3 api_runner "Emailadresse" "Passwort"'


    Requirements
    ------------
    - Windows (probably works on MacOS/Linux if the module winsound is removed, not tested)
    - Python 3.6+
    - To use Selenium to extract the OAuth nonce:
        - Firefox
        - Geckodriver (copy to main.py folder)
        - Configure path to Geckodriver, see 'root_folder' and 'webdriver_executable'
    - Dependencies from requirements.txt
    - See "ToDo" tags further down for configurations to adapt

    Copyright (c) 2021 Karsten Hoffrath (khoffrath). All rights reserved.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

    The latest version can be found here: https://gitlab.com/khoffrath/covid19-impf-o-mat

    Code quality: PoC ¯\_(ツ)_/¯
"""
import datetime
import os
import pytz
import random
import sys
import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

import api_client

# ToDo: Set path to folder where api_runner.py is located
# Only needed if Selenium is used
#root_folder = "c:/projects/impftermin/repo/python"
#webdriver_executable = os.path.join(root_folder, "geckodriver.exe")

#
# Don't touch anything below this line. Lowering sleep intervalls or
# timeouts result in worse performance.
#
wait_for_element_timeout = 10
wait_for_element_timeout_short = 1
wait_for_popup_timeout = 3
wait_for_element_poll_frequency = 0.1
sleep_intervall_error_retry = 3
sleep_intervall_smoothing = 0.5

nonce_lower_bound = 1100000000
nonce_upper_bound = 3700000000


# Extract nonce for a new session
def get_nonce(use_selenium=False):
    if not use_selenium:
        return random.randint(a=nonce_lower_bound, b=nonce_upper_bound)
 
    # Not used anymore. If you want to fetch the nonce from the website configure
    # the path webdriver_executable and call get_nonce(True)
    try:
        driver = webdriver.Firefox(executable_path=webdriver_executable)

        print('Loading homepage')
        driver.get('https://termin.corona-impfung.nrw/home')
        try:
            WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency)\
                .until(EC.presence_of_element_located((By.XPATH, '/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-startseite/div/div[2]/nav/a[2]')))
        except TimeoutException:
            print('Loading homepage too much time!')
            return None

        print('Checking for cookie banner')
        button_cookie_banner = driver.find_element_by_xpath('/html/body/ngb-modal-window/div/div/app-confirm-cookies-dialog/div/button')

        if button_cookie_banner:
            print('Closing cookie banner')
            time.sleep(sleep_intervall_smoothing)
            button_cookie_banner.click()

        print('Searching button to login page')
        button_login_page = driver.find_element_by_xpath(
            '/html/body/app-root/app-main-layout/div/div/div/div[2]/main/app-startseite/div/div[2]/nav/a[2]')

        if not button_login_page:
            print("Couldn't find button to login page")
            return None

        print('Clicking button to login page')
        try:
            time.sleep(sleep_intervall_smoothing)
            button_login_page.click()
        except:
            print('Exception while clicking the button to the login page')
            return None

        print('Waiting for login page to load')
        try:
            WebDriverWait(driver, wait_for_element_timeout, wait_for_element_poll_frequency) \
                .until(EC.presence_of_element_located((By.XPATH,
                                                       '//*[@id="submitButton"]')))
            time.sleep(sleep_intervall_smoothing)
        except TimeoutException:
            print('Loading login page took too much time!')
            return None

        url = driver.current_url
        if not url:
            print("Couldn't extract url")
            return None

        print('Extracting nonce')
        index_start = url.find('nonce=')
        sub_url = url[index_start + len('nonce='):]
        index_stop = sub_url.find('&')
        nonce = sub_url[0:index_stop]
        print(f'Found nonce "{nonce}"')
        return nonce
    except:
        pass
    finally:
        if driver:
            driver.close()
    return None


def run(username, password, only_log_appointments):
    nonce = get_nonce(use_selenium=False)
    if not nonce:
        print("Couldn't extract nonce, aborting...")
        sys.exit(-1)

    earliest_timestamp = None
    # Optionally configure the earliest timestamp that should be accepted:
    # Uncomment the following lines and set the desired date and time
    #earliest_timestamp_naive = datetime.datetime(year=2021, month=5, day=31, hour=17, minute=0, second=0)
    #timezone = pytz.timezone('Europe/Berlin')
    # Check for DST: https://gist.github.com/dpapathanasiou/09bd2885813038d7d3eb#gistcomment-3528461
    #earliest_timestamp = timezone.localize(earliest_timestamp_naive)

    print('Earliest timestamp set: {0}'.format(earliest_timestamp))

    # Run the api client
    client = api_client.ApiClient(nonce, username, password, earliest_timestamp, only_log_appointments)
    client.execute()
    client_result = client.result
    print('Resulting state: {0}. Message: "{1}"'.format(client_result['state'],
                                                        client_result['message']))
    pass


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage:')
        print('python api_runner.py "Emailadresse" "Passwort" ["true" -> only log appointments]')
        sys.exit(1)
    only_log_appointments = False
    if len(sys.argv) == 4:
        only_log_appointments = True
    run(username=sys.argv[1],
        password=sys.argv[2],
        only_log_appointments=only_log_appointments)

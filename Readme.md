# Covid19 Impf-O-Mat

Pythonskripte zum Buchen von Covid19-Impfterminen auf der Website https://termin.corona-impfung.nrw/.

# Einführung
In diesem Repository befinden sich zwei Skripte:

* python-selenium verwendet die Bibliothek Selenium um einen Firefox-Browser fernzusteuern und einen Impftermin zu buchen
* python-api verwendet die REST-Api der Website um einen Termin zu buchen

Die Api-Version ist weniger von der Leistungsfähigkeit des Computers abhängig und wesentlich schneller, daher sollte sie bevorzugt werden. Nachfolgende wird auch nur die API-Version beschrieben.

---
**DISCLAIMER**

Die Verwendung dieser Skript erfolgt auf eigene Gefahr. Der Autor übernimmt keine Verantwortung für Fehler oder Probleme die sich aus der Verwendung dieser Skripte ergeben könnten.

---

# Funktionsweise

* Das Skript verwendet die übergebene E-Mail-Adresse und Passwort um sich mit der API zu identifizieren
* Vor der Terminsuche wird anhand der Einstellungen im Benutzerprofil geprüft, ob bereits ein Impftermin vergeben wurde und gemäß den aktuell geltenden Richtlinien ein Impftermin gebucht werden darf. Ist bereits ein Termin gebucht oder keine Terminbuchung möglich wird das Skript beendet. Ist dem Benutzerprofil eine zweite Person zugeordnet die impfberechtigt ist und die noch keinen Impftermin besitzt so wird die Terminsuche für diese zweite Person fortgesetzt.
* Darf ein Termin gebucht werden so wird im Anschluss das zugeordnete Impfzentrum ermittelt. Es ist nicht möglich Termine in nicht zugeordneten Impfzentren zu buchen.
* Sobald diese Informationen ermittelt wurden beginnt das Skript nach freien Impfterminen zu suchen. Hierzu wird in kurzen Abständen die Liste der freien Impftermine abgefragt.
* Wird ein freier Impftermin gefunden zu dem auch ein Termin für die Zweitimpfung verfügbar ist so versucht das Skript diese Termine zu buchen.
  * Schlägt die Buchung dieses Terminpaares fehl so wird wieder damit begonnen die List der freien Impftermine abzurufen.
  * Ist die Buchung erfolgreich so wird durch die Website automatisch eine E-Mail an die registrierte E-Mail-Adresse verschickt. Ist dem Benutzerprofil eine zweite Person zugeordnet die impfberechtigt ist und noch keinen Impftermin besitzt so wird die Terminsuche für diese zweite Person fortgesetzt.
* Das Skript wird automatisch beendet sobald für alle registrierten impfberechtigten Personen ein Impftermin gebucht wurde.
 
# Installation

Das Skript wurde nur unter dem Windows-Betriebssystem getestet, sollte aber auch unter MacOS und Linux einsetzbar sein.

* Laden Sie alle Dateien aus dem Ordner "python-api" herunter und speichern Sie sie in einem Ordner auf Ihrer Festplatte
* Um das Skript ausführen zu können muss Python 3.6 oder höher installiert sein. Downloadlink: https://www.python.org/ftp/python/3.8.10/python-3.8.10.exe. Wird bei der Installation gefragt ob Python in den Systempfad aufgenommen werden soll, muss diese Frage mit "Ja" beantwortet werden.
* Nach der Installation von Python müssen noch einige Module installiert werden damit das Skript funktionsfähig ist:
  * Eine Eingabeaufforderung öffnen und in das Verzeichnis wechseln in dem die heruntergeladenen Dateien gespeichert wurden
  * Den Befehl `pip install -r requirements.txt` eingeben

# Ausführung

* Um das Skript auszuführen eine Eingabeaufforderung öffnen und in das Verzeichnis wechseln in das die Skriptdateien heruntergeladen wurden
* Den Befehl `python api_runner.py "E-Mailadresse" "Passwort"` eingeben und mit der Eingabetaste bestätigen
  * An den Stellen `E-Mailadresse`und `Passwort` sind die E-Mail-Adresse und das Passwort zur Anmeldung an der Website https://termin.corona-impfung.nrw/ anzugeben.
  * Um Probleme mit Sonderzeichen zu vermeiden sollten die E-Mail-Adresse und das Passwort wie im Beispiel gezeigt in Anführungszeichen gesetzt werden
 
Sind die Zugangsdaten korrekt und das Skript kann erfolgreich mit der Website kommunizieren werden in regelmäßigen Abständen Zeilen mit der aktuellen Uhrzeit und dem Text "Waiting for available days" ausgegeben.

Wurde ein Impftermin gebucht so wird eine Meldung ausgegeben und das Skript fortgesetzt, sofern eine zweite impfberechtigte Person gefunden wurde. Wurden für alle berechtigten Personen Impftermine gebucht so beendet sich das Skript automatisch und die Eingabeaufforderung kann geschlossen werden.

Soll die Terminsuche pausiert werden so kann das Skript durch Drücken der Tasten `Strg + c` unterbrochen und später durch erneute Eingabe von `python api_runner.py "E-Mailadresse" "Passwort"` neugestartet werden. Durch die beim Start ausgeführten Plausibilitätsprüfungen kann es auch bei wiederholten Ausführungen nicht zu Problemen kommen.


# Fragen

## Funktioniert es?

Die aktuelle Version funktioniert mit der Version der Website vom 30.05.2021. Wird am 07.06.2021 die Impfpriorisierung aufgehoben kann es zu Änderungen auf der Website kommen, die dazu führen, dass das Skript nicht mehr funktioniert. Sicherheitshalber bitte am _07.06.2021_ unter der folgenden Adresse prüfen ob eine neue Version zur Verfügung steht: https://gitlab.com/khoffrath/covid19-impf-o-mat.


## Probleme beim Impftermin

Bitte stellen Sie sicher, dass die Einstellungen der Impfpriorisierung in Ihrem Profil korrekt eingestellt sind. Wird im Impfzentrum festgestellt dass Sie nicht berechtigt geimpft zu werden liegt dies in Ihrer eigenen Verantwortung.
Die Termine werden über die Schnittstelle der offiziellen Website erstellt und sind von Terminen, die mit Hilfe eines Browsers erzeugt wurden nicht zu unterscheiden.

Sie verwenden die Skripte auf eigene Gefahr. Es wird keinerlei Verantwortung für Probleme die sich aus Verwendung der Skripte ergeben übernommen. 

## Sicherheit/Daten

Ihre E-Mail-Adresse und Passwort werden nur für die Anmeldung an der Impfwebsite und zur Buchung von Impfterminen verwendet. Sie werden weder an andere Personen, Firmen oder Server übertragen noch für andere Zwecke verwendet. Sie verlassen nicht Ihren Computer.

## Kurzfristige Termine

Häufig kommt es vor das sehr kurzfristige Termine vereinbart werden, beispielsweise gegen 21:00 Uhr wird ein Termin für den folgenden Morgen 8:45 Uhr gebucht. Bei Einsatz dieses Skriptes kontrollieren Sie bitte regelmäßig ob und wann Impftermine gebucht werden.
Für Experten ist es möglich einen Zeitpunkt anzugeben ab dem freie Termine berücksichtigt werden. Hierzu kann in der Datei "api_runner.py" in der Methode "run" ein Zeitstempel angegeben werden der zur Prüfung der gefundenen freien Termine verwendet wird. Bitte bedenken Sie, dass eine solche Einschränkung die Chancen auf einen Impftermin deutlich reduzieren können.

## Skriptmodifikationen

Werden die Timingzeiten im Skript verändert so wird dies zu einer insgesamt schlechteren Performance des Skriptes führen. Bei Tests wurde festgestellt das eine Verringerung der Sleep-Intervalle zu Verbindungsabbrüchen führt, welche einen kompletten Neustart des Kommunikationsprozesses zur Folge haben.